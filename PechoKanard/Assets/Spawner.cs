﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    [SerializeField] private Direction _facingDir;
    [SerializeField] private GameObject _objectToSpawn;

    [SerializeField] private float _minTimeBeforeSpwawn = 2;
    [SerializeField] private float _maxTimeBeforeSpawn = 4;

    private float _timer = 0;
    private float _nextSpawnTime = 0;


    // Update is called once per frame
    void Update()
    {

        _timer += Time.deltaTime;

        if (_timer >= _nextSpawnTime)
        {
            _nextSpawnTime = Time.time + Random.Range(_minTimeBeforeSpwawn, _maxTimeBeforeSpawn);
            SpawnObject();
        }
    }

    private void SpawnObject()
    {
      GameObject go =  Instantiate(_objectToSpawn, transform.position, Quaternion.identity, transform);

        Civilian civilian;

        if (civilian = go.GetComponent<Civilian>())
        {
            civilian.SetDirection(_facingDir);
        }
    }


}
