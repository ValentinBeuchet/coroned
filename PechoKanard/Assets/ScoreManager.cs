﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : MonoBehaviourSingleton<ScoreManager>
{
    [SerializeField] private int _scoreRedZone, _scoreYellowZone, _scoreGreenZone;
    [SerializeField] private Text _textScorePlayer, _textScoreOpponent;
    [SerializeField] private GameObject _redFeedback, _blueFeedback;

    public bool IsPlayerAhead => _scorePlayer >= _scoreOpponent;

    private bool _wasPlayerAlreadyAhead = true;

    private int _scorePlayer;
    private int _scoreOpponent;

    private bool _isPlayerTurn = true;
        
   public void AddScore(ScoreZone zone, Vector3 hitPoint)
    {
        int scoreToAdd= 0;

        switch (zone)
        {
            case ScoreZone.Red:
                scoreToAdd = _scoreRedZone;
                break;
            case ScoreZone.Yellow:
                scoreToAdd = _scoreYellowZone;
                break;
            case ScoreZone.Green:
                scoreToAdd = _scoreGreenZone;
                break;
            case ScoreZone.Special:
                break;
            default:
                break;
        }

        Vector3 v = hitPoint - Vector3.forward * 0.5f + Vector3.up * 1.5f;
        GameObject go;

        if (_isPlayerTurn == true)
        {
            _scorePlayer += scoreToAdd;
            _textScorePlayer.text = _scorePlayer.ToString();
            LeanTween.scale(_textScorePlayer.gameObject, Vector3.one * 1.2f, 0.08f).setLoopPingPong(1);

            go = Instantiate(_redFeedback, v, Quaternion.identity);
        }

        else
        {
            _scoreOpponent += scoreToAdd;
            _textScoreOpponent.text = _scoreOpponent.ToString();
            go = Instantiate(_blueFeedback, v, Quaternion.identity);
        }

        go.GetComponent<TextMeshPro>().text = "+" + scoreToAdd;
        LeanTween.moveLocalY(go, go.transform.position.y + 0.8f, 0.3f).setOnComplete(c => LeanTween.scale(go, Vector3.zero, 0.2f).setEaseInBack().setDelay(0.1f));


        SwitchTurn();

        if (_wasPlayerAlreadyAhead == true && IsPlayerAhead == false)
        {
            CrownManager.Instance.SwitchCrown();
            _wasPlayerAlreadyAhead = false;
        }

        else if (_wasPlayerAlreadyAhead == false && IsPlayerAhead)
        {
            CrownManager.Instance.SwitchCrown();
            _wasPlayerAlreadyAhead = true;
        }
    }

    public void SwitchTurn()
    {
        _isPlayerTurn = !_isPlayerTurn;
    }
}

public enum ScoreZone
{
    Red,
    Yellow,
    Green,
    Special
}
