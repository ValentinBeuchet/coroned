﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class CrownManager : MonoBehaviourSingleton<CrownManager>
{
    [SerializeField] private Transform _crownParentPlayer, _crownParentOpponent;
    [SerializeField] private Transform _crownObject;

    private bool _crownWasOnPlayer = true;

    private Vector3 _localOffset;

    private void Start()
    {
        _localOffset = _crownObject.localPosition;
    }


    [Button("SWITCH CROWN")]
    public void SwitchCrown()
    {
        if (_crownWasOnPlayer == true)
        {
            LeanTween.scale(_crownObject.gameObject, Vector3.zero, 0.08f).setEaseInBack().setOnComplete(c =>
            {
                _crownObject.parent = _crownParentOpponent;
                _crownObject.localPosition = _localOffset;
                _crownObject.eulerAngles = Vector3.zero;
                LeanTween.scale(_crownObject.gameObject, Vector3.one, 0.08f).setEaseOutBack().setOnComplete(v => _crownWasOnPlayer = false);
            });
        }

        else
        {
            LeanTween.scale(_crownObject.gameObject, Vector3.zero, 0.08f).setEaseInBack().setOnComplete(c =>
            {
                _crownObject.parent = _crownParentPlayer;
                _crownObject.localPosition = _localOffset;
                _crownObject.eulerAngles = Vector3.zero;
                LeanTween.scale(_crownObject.gameObject, Vector3.one, 0.08f).setEaseOutBack().setOnComplete(v => _crownWasOnPlayer = true);


            });
        }
    }

  
}
