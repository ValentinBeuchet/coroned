﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Civilian : MonoBehaviour
{

    [Header("INFECTION PARAMS")]
    [SerializeField] Material _safeMaterial;
    [SerializeField] Material _unsafeMaterial;

    [Header("MOVEMENT PARAMS")]
    [SerializeField] private Direction _moveDirection;
    [SerializeField] private float _moveSpeed;

    private bool _isInfected;


    // Start is called before the first frame update
    void Start()
    {
        SetInfection();
    }

    private void SetInfection()
    {

        MeshRenderer renderer = GetComponent<MeshRenderer>();

        if (Random.Range(0, 2) == 1)
        {
            _isInfected = true;
            renderer.material = _unsafeMaterial;
        }

        else
        {
            _isInfected = false;
            renderer.material = _safeMaterial;
        }

    }

    private void Move()
    {
        if (_moveDirection == Direction.Forward)
        {
            transform.position += Vector3.forward * _moveSpeed * Time.deltaTime;
        }

        else if(_moveDirection == Direction.Backward)
        { 
            transform.position -= Vector3.forward * _moveSpeed * Time.deltaTime;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    public void SetDirection(Direction dir)
    {
        _moveDirection = dir;
    }
}

public enum Direction
{
    Forward,
    Backward
}
