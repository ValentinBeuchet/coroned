﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;
using UnityEditor;

public enum GameState {
    INITIAL_STATE,
    IN_GAME,
    IN_GAME_REVIVED,
    GAME_OVER,
    LEVEL_SUCCESS,
    ASK_REVIVE
}
public class GameManager : MonoBehaviour {

    [SerializeField] private bool _isUsingCameraTransition;
    public bool IsUsingCameraTransition => _isUsingCameraTransition;

    private const string CURRENT_LEVEL_KEY = "CurrentLevel";

    public delegate void GameStartedDelegate ();
    public delegate void GameEndedDelegate ();
    public delegate void ScoreChangedDelegate (int n);
    public static event GameStartedDelegate OnGameStarted;
    public static event GameStartedDelegate OnGameEnded;
    public static event ScoreChangedDelegate OnScoreUpdate;
    

    static public GameManager Instance;

    private int currentLevelIndex;
    public int CurrentLevelIndex => currentLevelIndex;

    public GameState CurrentGameState = GameState.INITIAL_STATE;
    public bool InGame {
        get {
            return CurrentGameState == GameState.IN_GAME;
        }
    }

    private int m_CurrentScore = 0;
    public int CurrentScore {
        get {
            return m_CurrentScore;
        }

        set
        {
            m_CurrentScore = value;
        }
    }

    protected virtual void Awake ()
    {
        CurrentGameState = GameState.INITIAL_STATE;
        Time.timeScale = 1f;
        Application.targetFrameRate = 60;
        Instance = this;
        StartSession();
    }

    void Update()
    {

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel(Application.loadedLevel);
        }
#endif
    }

    protected void TriggerGameEnded()
    {
        if (OnGameEnded != null)
            OnGameEnded();
    }

    public void StartSession () {

        currentLevelIndex = PlayerPrefs.GetInt(CURRENT_LEVEL_KEY, 0);
        print("STARTING THE SESSION");
        if (CurrentGameState == GameState.IN_GAME)
            return;
        CurrentGameState = GameState.IN_GAME;

      

        if (OnGameStarted != null)
            OnGameStarted ();
        UIController.Instance.ShowPanel (UIPanelName.GAMEPLAY);

    }

    public void Revive () {
        // TODO : Revive Player
    }

    public void GameOver () {
        
        // If player has not been revived already
        if (CurrentGameState == GameState.IN_GAME) {
            // If ads are setup and rewarded is available :
#if MONETISED
            if (MonetizationManager.Instance != null && MonetizationManager.Instance.isRewardedAdAvailable) {
                // we offer a revive instead of Game Over
                UIController.Instance.ShowPanel (UIPanelName.ASK_REVIVE);
                CurrentGameState = GameState.ASK_REVIVE;
                return;
            }
#endif
        }

        // CLASSIC GAME OVER SITUATION
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "0");
#if MONETISED
        MonetizationManager.Instance.ShowInterstitial();
#endif
        UIController.Instance.ShowPanel (UIPanelName.GAME_OVER);
        CurrentGameState = GameState.GAME_OVER;
      //  FT_CoinManager.Instance.coinDisplayerPanel.SetActive(true);
        VibrationManager.VibrateFailure ();

        if (OnGameEnded != null)
            OnGameEnded ();

        System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
        if(LevelManager.Instance!=null)
            richEvent.Add("af_levelIndex", (LevelManager.Instance.CurrentLevelIndex).ToString());
        AppsFlyer.trackRichEvent("af_Progress_LevelFailed", richEvent);

    }

    public void LevelSuccess () {

        UIController.Instance.ShowPanel (UIPanelName.LEVEL_SUCCESS);
        CurrentGameState = GameState.LEVEL_SUCCESS;
        if (LevelManager.Instance != null) {
            LevelManager.Instance.LevelUp ();
        }

        PlayerPrefs.SetInt(CURRENT_LEVEL_KEY, currentLevelIndex + 1);
        VibrationManager.VibrateSuccess ();

        if (OnGameEnded != null)
            OnGameEnded ();

        Application.LoadLevel(Application.loadedLevel);

    }

    public virtual void Retry () {
        Time.timeScale = 1f;
        Application.LoadLevel (Application.loadedLevel);
        

    }

    public virtual void AddScore (int i) {
        m_CurrentScore += i;

        if (OnScoreUpdate != null)
            OnScoreUpdate (m_CurrentScore);

    }

   
}