﻿    using System.Collections.Generic;
    using System.Collections;
    using UnityEngine;

    public class LevelManager : MonoBehaviour {
        const string Key_CurrentLevel = "LM_CurrentLevel";

        public static LevelManager Instance;

        public int CurrentLevelIndex;

        protected bool OnFire = false;

      
        // Use this for initialization
        public virtual void Awake () {

        Instance = this;
        //SetFire (true);
        //SetFire (false);

        CurrentLevelIndex = PlayerPrefs.GetInt(Key_CurrentLevel, 1);
        }

        // Update is called once per frame
        void Update () {
        if (Input.GetKeyDown(KeyCode.L)) 
        {
            LevelUp();
         }

    }

        public void LevelUp () {

            CurrentLevelIndex++;
            PlayerPrefs.SetInt (Key_CurrentLevel, CurrentLevelIndex);
        System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
        richEvent.Add("af_levelIndex",(CurrentLevelIndex-1).ToString());
        AppsFlyer.trackRichEvent("af_Progress_LevelSuccess", richEvent);
    }


        public void StartLevel (int i) {
            CurrentLevelIndex = i;
           
        }

     
   


    }