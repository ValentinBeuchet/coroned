﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class DEBUG_PlayerPrefs : MonoBehaviour
{

	public string Key;

    public typeValue type;

    public enum typeValue
    {
        Int,
        Float,
        String,
    }

    [ShowIf("IsInt")]
    public int ValueInt;
    private bool IsInt() { return type == typeValue.Int; }
    [ShowIf("IsFloat")]
    public float ValueFloat;
    private bool IsFloat() { return type == typeValue.Float; }
    [ShowIf("IsString")]
    public string ValueString;
    private bool IsString() { return type == typeValue.String; }

    [ContextMenu ("Set Int")]
	public void SetInt ()
	{
		Debug.Log ("Setting Key : " + Key + " with value : " + ValueInt.ToString ());
		PlayerPrefs.SetInt (Key, ValueInt);
	}

	[ContextMenu ("Set Float")]
	public void SetFloat ()
	{
		Debug.Log ("Setting Key : " + Key + " with value : " + ValueFloat.ToString ());
		PlayerPrefs.SetFloat (Key, ValueFloat);
	}
    
	public void SetString ()
	{
		Debug.Log ("Setting Key : " + Key + " with value : " + ValueString);
		PlayerPrefs.SetString (Key, ValueString);
	}

    [Button("Set Value to Prefabs")]
    public void SetValue()
    {
        if (Key == "")
        {
            Debug.LogError("Forgot to set the Key");
            return;
        }

        switch (type)
        {
            case typeValue.Int:
                Debug.LogError("Setting Key : " + Key + " with value : " + ValueInt.ToString());
                //if(PlayerPrefs.HasKey(Key))
                PlayerPrefs.SetInt(Key, ValueInt);
                break;

            case typeValue.Float:
                Debug.Log("Setting Key : " + Key + " with value : " + ValueFloat.ToString());
                PlayerPrefs.SetFloat(Key, ValueFloat);
                break;

            case typeValue.String:
                Debug.Log("Setting Key : " + Key + " with value : " + ValueString);
                PlayerPrefs.SetString(Key, ValueString);
                break;
        }
    }

    [Button("Delete All")]
	public void DeleteAll ()
    {
        Debug.LogError("All Delete");
        PlayerPrefs.DeleteAll();
	}
}
