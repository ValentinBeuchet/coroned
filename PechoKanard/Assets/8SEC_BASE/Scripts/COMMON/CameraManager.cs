﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public static CameraManager Instance;
    public List<Camera> ActiveCameras = new List<Camera>();
    public Camera MainCamera;
    public Camera UICamera;


    private List<LayerMask> cullingMasks = new List<LayerMask>();
    private void Awake()
    {
        Instance = this;
        MainCamera = Camera.main;
        for (int i = 0; i < ActiveCameras.Count; i++)
        {
            cullingMasks.Add(ActiveCameras[i].cullingMask);
        }
    }
  
    public void SetCamerasEnabled(bool state)
    {
        for (int i = 0; i < ActiveCameras.Count; i++)
        {
            if( state)
                ActiveCameras[i].cullingMask = cullingMasks[i];

            else
                ActiveCameras[i].cullingMask = LayerMask.GetMask();
        }
    }

    public void SetCameraEnabled(int camIndex,bool state)
    {
        {
            if (state)
                ActiveCameras[camIndex].cullingMask = cullingMasks[camIndex];

            else
                ActiveCameras[camIndex].cullingMask = LayerMask.GetMask();
        }
    }


}
