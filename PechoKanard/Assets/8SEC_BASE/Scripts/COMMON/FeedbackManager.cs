﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackManager : MonoBehaviour {

    public static FeedbackManager Instance;

    void Awake() {
        Instance = this;
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
#if UNITY_EDITOR

#endif
    }

    public TextMeshPro TMPFeedback;
    readonly Color defaultFeedbackcolor = new Color(1f, 1F, 1F, 1f);
    public void Show3DTMPFeedback(string txt, Vector3 p, float duration) {
        Show3DTMPFeedback(txt, p, duration, Color.white);

    }

    public void ShowDunk3DTMPFeedback(string txt, Vector3 p, Vector3 offset, float duration, Color c, float scale = 1f, Transform parent = null) {

        var f = Instantiate(TMPFeedback, transform);
        f.gameObject.SetActive(true);
        if (parent != null)
            f.transform.SetParent(parent);

        f.text = txt;

        //LeanTween.cancel (MayhemFeedbackGO);
        f.transform.localScale = Vector3.zero;

        Vector3 res = p; //+new Vector3 (0f, 2f, 2f);
        res = res + offset;
        f.transform.eulerAngles = new Vector3(0f, 0f, Random.Range(-7.5f, 7.5f)); // p.x / 5f * 25f);
        //Debug.Log("text feedback : " + f.transform.localEulerAngles.ToString());

        //		res.x = Mathf.Clamp (res.x, -1f, 1f);
        f.transform.position = res;

        f.color = c;
        //f.transform.rotation = Player.Instance.transform.rotation;
        //f.transform.forward = Player.Instance.JoystickCam.transform.forward;
        //	MayhemFollowPlayer.StartFollow ();
        //  LeanTween.cancel (f.gameObject);
        LeanTween.scale(f.gameObject, Vector3.one * scale, 0.15f).setEase(LeanTweenType.easeOutBack);
        LeanTween.scale(f.gameObject, Vector3.zero, 0.15f).setDelay(0.15f + duration).setEase(LeanTweenType.easeInBack).setOnComplete(fs => {
            f.gameObject.SetActive(false);

        });

        if (parent != null)
            LeanTween.moveLocal(f.gameObject, new Vector3(0f, f.gameObject.transform.localPosition.y + 0.5f, f.gameObject.transform.localPosition.z), 0.3f);
        else
            LeanTween.move(f.gameObject, f.gameObject.transform.position + Vector3.up * 3f, 0.3f);
        //  LeanTween.alpha(f.gameObject, 0f, 0.5f).setDelay(duration).setEase(LeanTweenType.easeInBack);
    }

    public void Show3DFeedBack(string txt, Vector3 p, Vector3 offset, float duration, Color c, float scale = 1f, Transform parent = null) {

        var f = Instantiate(TMPFeedback, transform);
        f.gameObject.SetActive(true);
        if (parent != null)
            f.transform.SetParent(parent);

        f.text = txt;

        //LeanTween.cancel (MayhemFeedbackGO);
        f.transform.localScale = Vector3.zero;

        //f.transform.eulerAngles = new Vector3(0f, 0f, Random.Range(-7.5f, 7.5f)); // p.x / 5f * 25f);
        //Debug.Log("text feedback : " + f.transform.localEulerAngles.ToString());

        //		res.x = Mathf.Clamp (res.x, -1f, 1f);
        f.transform.position = p;

        f.color = c;
        //f.transform.rotation = Player.Instance.transform.rotation;
        //f.transform.forward = Player.Instance.JoystickCam.transform.forward;
        //	MayhemFollowPlayer.StartFollow ();
        //  LeanTween.cancel (f.gameObject);
        LeanTween.scale(f.gameObject, Vector3.one * scale, 0.15f).setEase(LeanTweenType.easeOutBack);
        LeanTween.scale(f.gameObject, Vector3.zero, 0.15f).setDelay(0.15f + duration).setEase(LeanTweenType.easeInBack).setOnComplete(fs => {
            f.gameObject.SetActive(false);

        });

        if (parent != null)
            LeanTween.moveLocal(f.gameObject, f.gameObject.transform.localPosition + Vector3.right * offset.x + Vector3.up * offset.y + Vector3.forward * offset.z, duration);
        else
            LeanTween.move(f.gameObject, f.gameObject.transform.position + offset, duration);
        //  LeanTween.alpha(f.gameObject, 0f, 0.5f).setDelay(duration).setEase(LeanTweenType.easeInBack);
    }

    public void Show3DFeedBackScaling(string txt, Vector3 p, Vector3 offset, float duration, Color c, float scale = 1f, Transform parent = null) {
        var f = Instantiate(TMPFeedback, transform);
        f.gameObject.SetActive(true);
        if (parent != null)
            f.transform.SetParent(parent);

        f.text = txt;

        f.transform.localScale = Vector3.zero;

        f.transform.position = p;

        f.color = c;
        LeanTween.scale(f.gameObject, Vector3.one * scale, duration).setOnComplete(fs => {
            LeanTween.scale(f.gameObject, Vector3.zero, 1f).setOnComplete(fs2 => {
                Destroy(f.gameObject);
            });

        });

        if (parent != null)
            LeanTween.moveLocal(f.gameObject, f.gameObject.transform.localPosition + Vector3.right * offset.x + Vector3.up * offset.y + Vector3.forward * offset.z, duration + 1);
        else
            LeanTween.move(f.gameObject, f.gameObject.transform.position + offset, duration + 1);
    }

    public void Show3DTMPFeedback(string txt, Vector3 p, float duration, Color c, float scale = 1f, Transform parent = null) {

        var f = Instantiate(TMPFeedback, transform);
        f.gameObject.SetActive(true);
        if (parent != null)
            f.transform.SetParent(parent);

        f.text = txt;

        //LeanTween.cancel (MayhemFeedbackGO);
        f.transform.localScale = Vector3.zero;

        Vector3 res = p; //+new Vector3 (0f, 2f, 2f);
        res.x += Random.Range(-1f, 1f);
        res.y += Random.Range(0f, 1f);
        f.transform.localEulerAngles = new Vector3(0f, 0f, Random.Range(-7.5f, 7.5f)); // p.x / 5f * 25f);
        //Debug.Log("text feedback : " + f.transform.localEulerAngles.ToString());

        //		res.x = Mathf.Clamp (res.x, -1f, 1f);
        f.transform.position = res;

        f.color = c;
        //f.transform.rotation = Player.Instance.transform.rotation;
        //f.transform.forward = Player.Instance.JoystickCam.transform.forward;
        //	MayhemFollowPlayer.StartFollow ();
        //  LeanTween.cancel (f.gameObject);
        LeanTween.scale(f.gameObject, Vector3.one * scale, 0.15f).setEase(LeanTweenType.easeOutBack);
        LeanTween.scale(f.gameObject, Vector3.zero, 0.15f).setDelay(0.15f + duration).setEase(LeanTweenType.easeInBack).setOnComplete(fs => {
            f.gameObject.SetActive(false);

        });

        if (parent != null)
            LeanTween.moveLocal(f.gameObject, f.gameObject.transform.localPosition + Vector3.up * 2f, 0.3f);
        else
            LeanTween.move(f.gameObject, f.gameObject.transform.position + Vector3.up * 2f, 0.3f);
        //  LeanTween.alpha(f.gameObject, 0f, 0.5f).setDelay(duration).setEase(LeanTweenType.easeInBack);
    }

    public void Show3DFeedback(string txt, float duration, Color c, float scale = 1f) {
        var f = Instantiate(TMPFeedback, TMPFeedback.transform.position, TMPFeedback.transform.rotation, TMPFeedback.transform.parent);
        f.gameObject.SetActive(true);

        f.text = txt;
        Vector3 savedPos = TMPFeedback.transform.position;
        //LeanTween.cancel (MayhemFeedbackGO);
        f.transform.localScale = Vector3.zero;

        Vector3 res = f.transform.position; //+new Vector3 (0f, 2f, 2f);
        res.x += Random.Range(-1f, 1f);
        //res.y += Random.Range(0f, 1f);
        //TMPFeedback.transform.localEulerAngles = new Vector3(0f, 0f, Random.Range(-7.5f, 7.5f)); // p.x / 5f * 25f);
        //Debug.Log("text feedback : " + f.transform.localEulerAngles.ToString());

        //		res.x = Mathf.Clamp (res.x, -1f, 1f);
        f.transform.position = res;

        f.color = c;
        //f.transform.rotation = Player.Instance.transform.rotation;
        //f.transform.forward = Player.Instance.JoystickCam.transform.forward;
        //	MayhemFollowPlayer.StartFollow ();
        //  LeanTween.cancel (f.gameObject);
        LeanTween.scale(f.gameObject, Vector3.one * scale, 0.15f).setEase(LeanTweenType.easeOutBack);
        LeanTween.scale(f.gameObject, Vector3.zero, 0.15f).setDelay(0.15f + duration).setEase(LeanTweenType.easeInBack).setOnComplete(fs => {
            Destroy(f.gameObject);
        });

        LeanTween.moveLocal(f.gameObject, TMPFeedback.gameObject.transform.localPosition + Vector3.up * 2f, 0.3f).setLoopPingPong(1);

    }


    public void Show3DFeedbackBouncing(string txt, Vector3 p, Vector3 offset, float duration, Color c, float scale = 1f, Transform parent = null) {
        var f = Instantiate(TMPFeedback, transform);
        f.gameObject.SetActive(true);
        if (parent != null)
            f.transform.SetParent(parent);

        f.text = txt;

        f.transform.localScale = Vector3.zero;

        f.transform.position = p;

        f.color = c;
        LeanTween.scale(f.gameObject, Vector3.one * scale, duration * 0.8f).setEaseInSine().setEaseOutBack().setOnComplete(fs => {
            LeanTween.scale(f.gameObject, Vector3.zero, duration * 0.2f).setOnComplete(fs2 => {
                Destroy(f.gameObject);
            });

        });

        if (parent != null)
            LeanTween.moveLocal(f.gameObject, f.gameObject.transform.localPosition + Vector3.right * offset.x + Vector3.up * offset.y + Vector3.forward * offset.z, duration * 0.8f).setEaseOutBounce();
        else
            LeanTween.move(f.gameObject, f.gameObject.transform.position + offset, duration * 0.8f).setEaseOutBounce();
    }

    public ParticleSystem ExplosionParticles;
    public void PlayExplosion(Vector3 position) {
        ExplosionParticles.transform.position = position;
        ExplosionParticles.Play();
    }

    public ParticleSystem bounceParticles;
    public void PlayBounceParticles(Vector3 position, Vector3 offset, Quaternion rotation) {
        if (bounceParticles == null) {
            return;
        }

        bounceParticles.transform.rotation = rotation;
        bounceParticles.transform.position = position - offset;
        bounceParticles.Play();
    }

    public ParticleSystem SpawnFX;
    private List<ParticleSystem> listFXSpawn = new List<ParticleSystem>();
    public void PlaySpawnFX(Vector3 position) {
        listFXSpawn.Add(Instantiate(SpawnFX, position, Quaternion.identity));
        listFXSpawn[listFXSpawn.Count - 1].Play();
        Invoke("DestroyFXSpawn", 0.5f);
    }

    private void DestroyFXSpawn() {
        Destroy(listFXSpawn[0].gameObject);
        listFXSpawn.RemoveAt(0);
    }

    public ParticleSystem WaterSplashFX;
    private List<ParticleSystem> listWaterSplashFX = new List<ParticleSystem>();
    public void PlayWaterFX(Vector3 position) {
        listWaterSplashFX.Add(Instantiate(WaterSplashFX, position, Quaternion.identity));
        listWaterSplashFX[listWaterSplashFX.Count - 1].Play();
        Invoke("DestroyFXWater", 0.5f);
    }

    private void DestroyFXWater() {
        Destroy(listWaterSplashFX[0].gameObject);
        listWaterSplashFX.RemoveAt(0);
    }


    public ParticleSystem deSpawnSheeps;
    private List<ParticleSystem> ListdeSpawnSheeps = new List<ParticleSystem>();
    public void PlayDeSpawnSheep(Vector3 position) {
        ListdeSpawnSheeps.Add(Instantiate(deSpawnSheeps, position, Quaternion.identity));
        ListdeSpawnSheeps[ListdeSpawnSheeps.Count - 1].Play();
        Invoke("DestroyDespawnSheep", 0.5f);
    }

    private void DestroyDespawnSheep() {
        Destroy(ListdeSpawnSheeps[0].gameObject);
        ListdeSpawnSheeps.RemoveAt(0);
    }

    //Power Between 0 and 1
    public void PlayBounceParticles(Vector3 position, Vector3 offset, Quaternion rotation, int powerParticule) {
        if (bounceParticles == null)
            return;

        bounceParticles.transform.rotation = rotation;
        bounceParticles.transform.position = position - offset;
        ParticleSystem.MainModule main = bounceParticles.main;
        main.maxParticles = powerParticule;

        float ToAdd = 0.1f * ((float)powerParticule / 250f);
        main.startSize = new ParticleSystem.MinMaxCurve(0.1f + ToAdd, 0.2f + ToAdd);

        bounceParticles.Play();
    }

    public RectTransform flashImage;
    private int idFlash = -1;
    public void Flash() {
        LeanTween.cancel(idFlash);
        Color tmpColor = flashImage.GetComponent<Image>().color;
        tmpColor.a = 0;
        flashImage.GetComponent<Image>().color = tmpColor;
        LeanTween.alpha(flashImage, 1f, 0.05f).setLoopPingPong(1);
    }

}