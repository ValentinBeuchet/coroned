﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleFXButton : MonoBehaviour {

	public void Action () {
		if (SoundManager.Instance != null) {
			SoundManager.Instance.ToggleSound ();
		}
	}
}