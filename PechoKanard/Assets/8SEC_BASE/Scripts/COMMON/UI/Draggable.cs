﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Draggable : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerUpHandler, IPointerClickHandler, IDropHandler, IPointerEnterHandler, IPointerExitHandler {

    public virtual void OnPointerDown (PointerEventData eventData) {

    }
    public virtual void OnBeginDrag (PointerEventData eventData) {

    }

    public virtual void OnDrag (PointerEventData eventData) {

    }

    public virtual void OnEndDrag (PointerEventData eventData) {

    }

    public virtual void OnPointerUp (PointerEventData eventData) {

    }

    public virtual void OnPointerClick (PointerEventData eventData) {

    }

    public virtual void OnDrop (PointerEventData eventData) {

    }

    public virtual void OnPointerEnter (PointerEventData eventData) {

    }

    public virtual void OnPointerExit (PointerEventData eventData) {

    }
}