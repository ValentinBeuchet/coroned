﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleAmbiantMusic : MonoBehaviour
{
    public Image ButtonImage;
    public Sprite ON;
    public Sprite OFF;



  

    public void Toggle()
    {
        if (SoundManager.Instance != null)
        {
            SoundManager.Instance.ToggleBackgroundMusic();

        }
        if (ButtonImage == null || ON == null || OFF == null)
        {
            Debug.LogError("ToggleBackgroundMusic : One or more public references aren't set. ");
            return;
        }
        if (UserConfig.BackgroundMusic)
        {
            ButtonImage.sprite = ON;
        }
        else
        {
            ButtonImage.sprite = OFF;

        }
    }
}
