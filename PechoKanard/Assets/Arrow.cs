﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Arrow : MonoBehaviour
{
    private bool _hasCollisioned;
    public bool HasCollisionned => _hasCollisioned;

    public void Freeze()
    {
        Camera.main.DOShakePosition(0.1f, 0.1f);
        _hasCollisioned = true;
        GetComponent<Rigidbody>().isKinematic = true;
    }
}
