﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DartPlayer : MonoBehaviourSingleton<DartPlayer>
{

    [SerializeField] private GameObject _dartModel;
    [SerializeField] private LayerMask _targetLayer;

    [SerializeField] private Transform _handPos;
    [SerializeField] private bool _isThrowingAxes;

    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void StartThrowAnim()
    {
        _animator.SetTrigger("Throw");
    }

    public void ThrowDartAnim()
    {
        Ray ray = new Ray(Camera.main.transform.position, TargetPoint.Instance.GetDir());

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            Vector3 dartEndPos = hit.point;

            Transform dart = Instantiate(_dartModel, _handPos.position, Quaternion.identity).transform;
            if (_isThrowingAxes)
            {
                dart.eulerAngles += Vector3.up * 180;
                LeanTween.rotateAroundLocal(dart.gameObject, dart.right, 650, 0.2f);
            }

            LeanTween.move(dart.gameObject, dartEndPos, 0.2f);
        }

    }
    

    public void ThrowDart(Vector3 dir)
    {
        Ray ray = new Ray(Camera.main.transform.position, dir);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100))
        {
            Vector3 dartEndPos = hit.point;

            Transform dart = Instantiate(_dartModel, _handPos.position, Quaternion.identity).transform;
            LeanTween.move(dart.gameObject, dartEndPos, 0.2f);
        }

       //Debug.DrawRay(Camera.main.transform.position, dir, Color.yellow, 100);
    }


}
