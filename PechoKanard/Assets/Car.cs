﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    [SerializeField] private float _carSpeed;

  
    // Update is called once per frame
    void Update()
    {
        Move(); 
    }

    private void Move()
    {
        transform.position += -Vector3.forward * _carSpeed * Time.deltaTime;
    }
}
