﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetScoreZone : MonoBehaviour
{
    [SerializeField] private ScoreZone _scoreZoneColor;
    [SerializeField] private GameObject _smokePuff;

    private void OnCollisionEnter(Collision collision)
    {
        Arrow arrow;

        if (arrow = collision.gameObject.GetComponent<Arrow>())
        {
            if (arrow.HasCollisionned)
            {
                return;
            }

            ScoreManager.Instance.AddScore(_scoreZoneColor, collision.contacts[0].point);
            Debug.LogError(collision.other + " hit " + " Color zone " + _scoreZoneColor);
            arrow.Freeze();

            Instantiate(_smokePuff, collision.contacts[0].point - Vector3.forward * 0.4f, Quaternion.identity);
        }
    }

   

}
