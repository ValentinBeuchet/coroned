﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class DartsCamBehaviour : MonoBehaviour
{
    [SerializeField] private float _camTransitionTime = 0.4f;
    [SerializeField] private Transform _originalCam, _shootCam;

    private void Update()
    {
        if (Input.GetMouseButtonDown(1) && GameManager.Instance.IsUsingCameraTransition)
        {
            SwitchCam();
        }
    }



    [Button("SWITCH CAM")]
   private void SwitchCam()
    {
        if (transform.parent == _originalCam)
        {
            transform.parent = _shootCam;
            LeanTween.moveLocal(gameObject, Vector3.zero, _camTransitionTime).setOnComplete( c => TargetPoint.Instance.StartMove());//.setEaseOutBack();
            LeanTween.rotateLocal(gameObject, Vector3.zero, _camTransitionTime);
        }

        else
        {
            transform.parent = _originalCam;
            LeanTween.moveLocal(gameObject, Vector3.zero, _camTransitionTime);//.setEaseOutBack();
            LeanTween.rotateLocal(gameObject, Vector3.zero, _camTransitionTime);
        }

      
    }
}
