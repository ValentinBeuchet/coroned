﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPoint : MonoBehaviourSingleton<TargetPoint>
{
    [SerializeField] private Transform _minX, _maxX, _minY, _maxY;
    [SerializeField] private LayerMask _layerTarget;


    [SerializeField] private float _moveTime;
    [SerializeField] private float _delayBeforeReset = 0.2f;

    [SerializeField] private GameObject _horizontalLine, _verticalLine;

    LTDescr _moveXLT;
    LTDescr _moveYLT;

    private bool _hasChosenX;

    private Vector3 _throwDir;




    private void Start()
    {
        if (GameManager.Instance.IsUsingCameraTransition == true)
        {
            GetComponent<MeshRenderer>().enabled = false;
        }

        else
        {
            StartMove();
        }
    }

    public void StartMove()
    {
        GetComponent<MeshRenderer>().enabled = true;
        transform.position = transform.position.SetX(_minX.position.x);
        _moveXLT = LeanTween.moveX(gameObject, _maxX.position.x, _moveTime).setLoopPingPong(-1);
        _verticalLine.SetActive(false);
        _horizontalLine.SetActive(true);
    }




    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (_hasChosenX == false)
            {
                LeanTween.cancel(_moveXLT.uniqueId);
                _throwDir.x = transform.position.x;
                _hasChosenX = true;
                _verticalLine.SetActive(true);
                _verticalLine.transform.position = _verticalLine.transform.position.SetX(transform.position.x);
                _horizontalLine.SetActive(false);

                _moveYLT = LeanTween.moveY(gameObject, _maxY.position.y, _moveTime * 0.5f).setOnComplete( c => LeanTween.moveY(gameObject, _minY.position.y, _moveTime).setLoopPingPong(-1));
               
            }

            else
            {
                LeanTween.cancel(_moveYLT.uniqueId);
                _throwDir.y = transform.position.y;
                _throwDir.z = transform.position.z;
                LeanTween.scale(transform.parent.gameObject, Vector3.zero, 0.2f).setEaseOutBack();

                //Vector3 actualDir = (_throwDir - Camera.main.transform.position).normalized;
                //DartPlayer.Instance.ThrowDart(actualDir);
                _verticalLine.SetActive(false);
                DartPlayer.Instance.StartThrowAnim();
                LeanTween.delayedCall(_delayBeforeReset, ResetAll);
            }
          

        }
    }

    public Vector3 GetDir()
    {
        return (_throwDir - Camera.main.transform.position).normalized;
    }

    public void ResetAll()
    {
        transform.position = _minX.position;
        _throwDir = Vector3.zero;
        _hasChosenX = false;
        _moveXLT = null;
        _moveYLT = null;
        _horizontalLine.SetActive(true);
        LeanTween.scale(transform.parent.gameObject, Vector3.one, 0.2f).setEaseOutBack().setOnComplete( c => _moveXLT = LeanTween.moveX(gameObject, _minX.position.x, _moveTime).setLoopPingPong(-1));
    }

    public Vector3 GetRandomPointOnTarget()
    {

        float randX = Random.Range(_minX.position.x, _maxX.position.x);
        float randY = Random.Range(_minY.position.y, _maxY.position.y);

        Ray ray = new Ray(new Vector3(randX, randY, transform.position.z), Vector3.forward);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100, _layerTarget))
        {
            return new Vector3(randX, randY, hit.point.z);
        }


        Debug.LogError("ERROR");
        return Vector3.zero;
    }
}
